package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.InputMismatchException;
import java.util.Scanner;

import interfaces.LoginInterface;

public class Cliente {
	
	public static void main(String[] args) {
		String nombreServicio;		
		Registry registry;
		LoginInterface login;
		int opcion = 0;
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		

		try {
			registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido.");
			
			nombreServicio = "Login";
			login = (LoginInterface) registry.lookup(nombreServicio);
			do {
				System.out.println("1. Iniciar sesi�n.");
				System.out.println("2. Registrarse.");
				System.out.print("Opcion: ");
				opcion = teclado.nextInt();
				
				switch(opcion) {
				case 1:
					System.out.println("INICIAR SESI�N");
					if(login.login(readUsername(), readPassword()) > 0) {
						System.out.println("Has iniciado sesi�n satisfactoriamente.");
						opcion = 3;
					}
					break;
				case 2:

					if(login.register(readUsername(), readPassword()) > 0) {
						System.out.println("Has sido registrado satisfactoriamente.");
						opcion = 0;
					}
					break;
				default:
					System.out.println("Elige una opci�n valida.");
				}
			} while(opcion >= 0 && opcion < 3);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (InputMismatchException e) {
			System.err.println("Introduce solo n�meros.");
			teclado = new Scanner(System.in);
		}
		
		System.out.println("Saliendo del programa...");
	}
	
	public static String readUsername() {
		String username = "";
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Nombre de usuario: ");
			username = teclado.nextLine();
		} while(username.isEmpty());
		
		return username;
	}
	
	public static String readPassword() {
		String password = "";
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Contrase�a: ");
			password = teclado.nextLine();
		} while(password.isEmpty());
		
		return password;
	}

}
