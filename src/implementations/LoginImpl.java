package implementations;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.TreeMap;

import interfaces.LoginInterface;
import models.Usuario;

public class LoginImpl implements LoginInterface, Serializable {

	private TreeMap<String, Usuario> usuarios;
	private static final long serialVersionUID = 3055350666331349196L;

	public LoginImpl() {
		this.usuarios = new TreeMap<>();
	}
	
	@Override
	public int register(String nickname, String password) throws RemoteException {
		int result = 0;
		
		if(nickname != null
				&& password != null
				&& !nickname.isEmpty()
				&& !password.isEmpty())
			if(isUserExist(nickname) == 0) {
				usuarios.put(nickname, new Usuario(nickname, password));
				result = 1;
			}
		
		return result;
	}

	@Override
	public int login(String nickname, String password) throws RemoteException {
		int result = 0;
		
		if(nickname != null
				&& password != null
				&& !nickname.isEmpty()
				&& !password.isEmpty())
			if(isLoginCorrect(nickname, password) > 0)
				result = 1;
		
		return result;
	}

	@Override
	public int isUserExist(String nickname) {
		int result = 0;
		
		if(usuarios.containsKey(nickname))
			result = 1;
		
		return result;
	}

	@Override
	public int isLoginCorrect(String nickname, String password) {
		int result = 0;
		Usuario usuario;
		
		if((usuario = usuarios.get(nickname)) != null) {
			usuario.getPassword().equals(password);
			result = 1;
		}
		
		return result;
	}

}
