package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LoginInterface extends Remote {
	
	int register(String nickname, String password) throws RemoteException;
	int login(String nickname, String password) throws RemoteException;
	int isUserExist(String nickname) throws RemoteException;
	int isLoginCorrect(String nickname, String password) throws RemoteException;

}
