package models;

import java.io.Serializable;

public class Usuario implements Serializable {

	private String nickname;
	private String password;
	private static final long serialVersionUID = -241940758011784797L;
	
	public Usuario() {
		this("", "");
	}

	public Usuario(String nickname, String password) {
		this.setNickname(nickname);
		this.setPassword(password);
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nickname == null) ? 0 : nickname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equals(other.nickname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [nickname=" + nickname + ", password=" + password + "]";
	}

}
