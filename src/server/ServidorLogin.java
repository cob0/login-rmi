package server;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import implementations.LoginImpl;
import interfaces.LoginInterface;

public class ServidorLogin {
	
	public static void main(String[] args) {
		Registry registry;
		String nombreServicio;
		LoginInterface loginInterface;
		LoginInterface stub;
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		
		try {
			registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
			System.out.println("Registro obtenido.");
			
			nombreServicio = "Login";
			
			loginInterface = new LoginImpl();
			stub = (LoginInterface) UnicastRemoteObject.exportObject(loginInterface, 0);
			System.out.println("Stub creado.");
			
			registry.rebind(nombreServicio, stub);
			System.out.println("Servicio a�adido.");
			
			System.out.println("Pulsar una tecla para terminar...");
			System.in.read();
			System.out.println("Finalizando servidor...");
			
			registry.unbind(nombreServicio);
			UnicastRemoteObject.unexportObject(loginInterface, true);
			System.out.println("Servidor finalizado.");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}

}
